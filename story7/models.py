from django.db import models
from datetime import date

# Create your models here.

class StatusModels(models.Model):
	description = models.CharField(max_length=300)
	date = models.DateField(auto_now_add=True)
