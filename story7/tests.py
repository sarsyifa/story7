from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, profilepage

from .models import StatusModels
from .forms import StatusForm
from selenium import webdriver
import time
import unittest
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.color import Color



# Create your tests here.
class Story6_test(TestCase):
	"""docstring for ClassName"""
	def test_url_is_active(self):
		response = Client().get('/story7/')
		self.assertEqual(response.status_code, 200)

	def test_appbang_using_index(self):
		found = resolve('/story7/')
		self.assertEqual(found.func, index)

	def test_contains_hello_apa_kabar(self):
		request = HttpRequest()
		response = index(request)
		html_response = response.content.decode('utf8')
		self.assertIn('Hello, apa kabar?', html_response)

	def test_validation_of_blank_form(self):
		form = StatusForm(data={'description': ''})
		self.assertFalse(form.is_valid())

	def test_model_can_create_new_status(self):
	# Creating a new activity
		new_status = StatusModels.objects.create(description = 'test ppw')
		count_status = StatusModels.objects.all().count()
		self.assertEqual(count_status, 1)


	def test_form_validation_max_300_character(self):
		form = StatusForm(data={'description': 'x'*310})
		self.assertFalse(form.is_valid())

	def test_profile_using_profile_func(self):
		found = resolve('/story7/profile/')
		self.assertEqual(found.func, profilepage)

class Story7FunctionalTest(TestCase):

	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')        
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(Story7FunctionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(Story7FunctionalTest, self).tearDown()

	def test_input_status(self):
		self.selenium.get('http://127.0.0.1:8000/story7')
		time.sleep(3)
		statusbox = self.selenium.find_element_by_name('description')
		statusbox.send_keys('Coba Coba')
		statusbox.submit()
		time.sleep(3)
		self.assertIn("Coba Coba", self.selenium.page_source)

	def test_check_the_button(self):
		self.selenium.get('http://127.0.0.1:8000/story7')
		time.sleep(3)
		button = self.selenium.find_element_by_tag_name('button').text
		self.assertIn('Submit', button)

	def test_check_the_title(self):
		self.selenium.get('http://127.0.0.1:8000/story7')
		time.sleep(3)
		title = self.selenium.find_element_by_tag_name('h1').text
		self.assertIn('Hello, apa kabar?', title)

	def test_check_topnav_color(self):
		self.selenium.get('http://127.0.0.1:8000/story7')
		time.sleep(3)
		navbar = self.selenium.find_element_by_id('nav').value_of_css_property('color')
		thecolor = Color.from_string(navbar).hex
		self.assertEqual('#212529', thecolor)


if __name__ == '__main__':
    unittest.main(warnings='ignore')




		