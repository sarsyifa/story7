from django.shortcuts import render, reverse
from django.http import HttpResponse, HttpResponseRedirect

# Create your views here.

from .models import StatusModels
from .forms import StatusForm

def index(request):
    hasil = StatusModels.objects.all()
    if request.method == 'POST':
        form = StatusForm(request.POST)
        if form.is_valid():
            description = request.POST['description']
            stat = StatusModels.objects.create(description=description)
            return render(request, 'index.html', {'form': form, 'description': hasil})
    else:
        form = StatusForm()
        return render(request, 'index.html', {'form': form, 'description': hasil})
'''response = {'author' : 'syifa'}

def index(request):
    form = StatusForm(request.POST or None)
    response['status_form'] = form
    # if this is a POST request we need to process the form data
    print(request.method)
    if request.method == 'POST':
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            response['description'] = request.POST['description']

            the_status = StatusModels(description=response['description'])
            the_status.save()

            return HttpResponseRedirect(reverse('appbang:allstatus'))

    # if a GET (or any other method) we'll create a blank form
    else:

        return render(request, 'index.html', response)

    

def allstatus(request):
    all_status = StatusModels.objects.all()
    response['all_status'] = all_status
    return render(request, 'updatestatus.html', response)'''


def profilepage(request):
    return render(request, 'profile.html')

