from django import forms
from datetime import date

class StatusForm(forms.Form):
	attrs = {
        'type': 'text',
        'cols': 50,
        'rows': 4,
        'class': 'todo-form-textarea',
        'placeholder':'Curhat disini yuk'
    }
	description = forms.CharField(label='', required=True, max_length=200, widget=forms.Textarea(attrs=attrs))
